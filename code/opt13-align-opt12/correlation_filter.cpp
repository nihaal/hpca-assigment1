#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <ctime>
#include <fstream>
using namespace std;

#define FILTER_SIZE 3
#define FILTERS 2

// Optimize this function
void applyFilters(int *image, int height, int width, int *filter, int filters)
{
    for(int i = 0; i < filters; ++i)
    {
        int *copy = new int[height * width];
		int indices[8] __attribute__((aligned(64)));
		//filterindex, filterrowindex, imagerowindex, imagecolindex, offxh, offyw, offx, offy
		indices[0] = i * FILTER_SIZE * FILTER_SIZE;
        
        for(int h = 0; h < height; ++h)
        {
			indices[2] = h * width;
			for(int w = 0; w < width; ++w)
            {
				indices[3] = indices[2] + w;
                copy[indices[3]] = 0;
                for(indices[6] = -1; indices[6] <= 1; ++(indices[6]))
                {
					indices[4] = indices[6] + h;
					if(indices[4] < 0 || indices[4] >= height)
						continue;
					indices[1] = indices[0] + (1+ indices[6]) * FILTER_SIZE;
					for(indices[7] = -1; indices[7] <= 1; ++indices[7])
                    {
						indices[5] = indices[7] + w;
                        if(indices[5] < 0 || indices[5] >= width)
                            continue;
                        copy[indices[3]] += image[(indices[4]) * width + indices[5]] * 
                            filter[indices[1] + 1 + indices[7]];
                    }
                }
            }
        }
        
	int *temp = image;
	image = copy;
	delete temp;
    }
}

int main()
{
    int ht, wd;
    cin >> ht >> wd;
    int *img = new int[ht * wd];
    for(int i = 0; i < ht; ++i)
        for(int j = 0; j < wd; ++j)
            cin >> img[i * wd + j];
            
    int filters = FILTERS;
    int *filter = new int[filters * FILTER_SIZE * FILTER_SIZE];
    for(int i = 0; i < filters; ++i)
        for(int j = 0; j < FILTER_SIZE; ++j)
            for(int k = 0; k < FILTER_SIZE; ++k)
                cin >> filter[i * FILTER_SIZE * FILTER_SIZE + j * FILTER_SIZE + k];
    
    clock_t begin = clock();
    applyFilters(img, ht, wd, filter, filters);
    clock_t end = clock();
    cout << "Execution time: " << double(end - begin) / (double)CLOCKS_PER_SEC << " seconds\n";
    ofstream fout("output.txt");
    for(int i = 0; i < ht; ++i)
    {
        for(int j = 0; j < wd; ++j)
            fout << img[i * wd + j] << " ";
        fout << "\n";
    }
}
