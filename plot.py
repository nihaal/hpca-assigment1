import sys
import matplotlib.pyplot as plt
import numpy as np

if len(sys.argv) < 3:
    print("usage: python plot.py list,of,optimizations,to,compare list,of,perf,events")
    sys.exit(0)

data = {}
for event in sys.argv[2].split(','):
    number = []
    std = []
    for opt in sys.argv[1].split(','):
        opt = int(opt.strip())
        arr = []
        with open("logs/opt"+str(opt)+"/numbers/"+event, "r") as file:
            for line in file:
                arr.append(float(line.strip()))
        nparr = np.array(arr)
        number.append(np.mean(nparr))
        std.append(np.std(nparr))
    data[event]=(number, std)

fig, ax = plt.subplots()
col = ["tab:blue", "tab:red"]
i=0
for event in sys.argv[2].split(','):
    (number, std) = data[event]
    ax.bar(np.arange(len(sys.argv[1].split(','))), number, yerr=std,width=0.1, alpha=0.5, color=col[i%2])
    i = i+1
ax.set_xticks(np.arange(len(sys.argv[1].split(','))))
ax.set_xticklabels(["opt"+i for i in sys.argv[1].split(',')])
ax.set_title(event)
ax.yaxis.grid(True)

plt.tight_layout()
plt.show()
