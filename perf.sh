#!/bin/bash
if [ $# -eq 1 ]
then
	echo "Deleting output.txt and old logs"
	rm output.txt
	rm logs/$1 -r
	mkdir logs/$1
	for i in {1..5}
	do
		echo "Performing perf stats, iteration:"$i
		perf stat -o logs/$1/stats$i -e 'cache-misses,cache-references,instructions,L1-dcache-load-misses' ./filter < input
	done
	#Create a way to view mean and SD of the stats
	#cat logs/$1/stats
	echo "Performing perf record"
	perf record ./filter < input
	mv perf.data logs/$1/
	echo "Diff of output"
	diff output.txt originalOutput.txt
else
	echo "Usage: bash perf.sh opt_name"
fi
