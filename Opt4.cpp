#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <ctime>
#include <fstream>
#include <emmintrin.h>
#include <smmintrin.h>
using namespace std;

#define FILTER_SIZE 3
#define FILTERS 2
#define FILTERINDEX indices[0]
#define HHHH indices[1]
#define IMAGEROWINDEX indices[2]
#define WWWW indices[3]
#define OFFXH indices[4]
#define OFFYW indices[5]
#define OFFX indices[6]
#define OFFY indices[7]

// Optimize this function
void applyFilters(int *image, int height, int width, int *filter, int filters)
{
	for(int i = 0; i < filters; ++i)
	{
		int *copy = new int[(height * width)];
		int indices[8] __attribute__((aligned(64)));
		//filterindex, h, imagerowindex, w, offxh, offyw, offx, offy
		FILTERINDEX = i * FILTER_SIZE * FILTER_SIZE;

		__m128i *frows = new __m128i[3];
		__m128i resultReg;
		frows[0] = _mm_set_epi32(filter[FILTERINDEX + 0], filter[FILTERINDEX + 1], filter[FILTERINDEX + 2], 0);
		frows[1] = _mm_set_epi32(filter[FILTERINDEX + 3], filter[FILTERINDEX + 4], filter[FILTERINDEX + 5], 0);
		frows[2] = _mm_set_epi32(filter[FILTERINDEX + 6], filter[FILTERINDEX + 7], filter[FILTERINDEX + 8], 0);

		int arr[4] __attribute__((aligned(32))); 
		int img[4] __attribute__((aligned(32)));
		img[3]=0;

		for(HHHH = 0; HHHH < height; ++HHHH)
		{
			IMAGEROWINDEX = HHHH * width;
			for(WWWW = 0; WWWW < width; ++WWWW)
			{
				resultReg = _mm_set1_epi32(0);
				for(OFFX = -1; OFFX <= 1; ++(OFFX))
				{
					OFFXH = OFFX + HHHH;
					if(OFFXH < 0 || OFFXH >= height){
						continue;
					}
					for(OFFY = -1; OFFY <= 1; ++OFFY)
					{
						OFFYW = OFFY + WWWW;
						if(OFFYW < 0 || OFFYW >= width){
							img[OFFY+1]=0;
							continue;
						}
						img[OFFY+1] = image[(OFFXH)*width + OFFYW];
					}
					//Multiply loaded image row with filter row
					resultReg = _mm_add_epi32(resultReg, _mm_mullo_epi32(_mm_set_epi32(img[0], img[1], img[2],0) ,frows[OFFX+1]));
				}
				//Add the result values and store in copy array
				_mm_storeu_si128((__m128i*)arr, resultReg);
				copy[IMAGEROWINDEX + WWWW] = arr[3] + arr[2] + arr[1];
			}
		}

		int *temp = image;
		image = copy;
		delete temp;
	}
}

int main()
{
	int ht, wd;
	cin >> ht >> wd;
	int *img = new int[ht * wd];
	for(int i = 0; i < ht; ++i)
		for(int j = 0; j < wd; ++j)
			cin >> img[i * wd + j];

	int filters = FILTERS;
	int *filter = new int[filters * FILTER_SIZE * FILTER_SIZE];
	for(int i = 0; i < filters; ++i)
		for(int j = 0; j < FILTER_SIZE; ++j)
			for(int k = 0; k < FILTER_SIZE; ++k)
				cin >> filter[i * FILTER_SIZE * FILTER_SIZE + j * FILTER_SIZE + k];

	clock_t begin = clock();
	applyFilters(img, ht, wd, filter, filters);
	clock_t end = clock();
	cout << "Execution time: " << double(end - begin) / (double)CLOCKS_PER_SEC << " seconds\n";
	ofstream fout("output.txt");
	for(int i = 0; i < ht; ++i)
	{
		for(int j = 0; j < wd; ++j)
			fout << img[i * wd + j] << " ";
		fout << "\n";
	}
}
