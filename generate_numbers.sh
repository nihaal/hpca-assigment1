#!/bin/bash
for i in {0..6}
do
	rm logs/opt$i/numbers -r
	mkdir logs/opt$i/numbers

	cat logs/opt$i/stats* | grep cache-misses | awk -e '{print $1}' | sed -e 's/,//g' > logs/opt$i/numbers/Cache-misses
	cat logs/opt$i/stats* | grep cache-references | awk -e '{print $1}' | sed -e 's/,//g' > logs/opt$i/numbers/Cache-references
	cat logs/opt$i/stats* | grep instructions | awk -e '{print $1}' | sed -e 's/,//g' > logs/opt$i/numbers/Instructions
	cat logs/opt$i/stats* | grep L1-dcache-load-misses | awk -e '{print $1}' | sed -e 's/,//g' > logs/opt$i/numbers/L1-dcache-load-misses
	cat logs/opt$i/stats* | grep time | awk -e '{print $1}' > logs/opt$i/numbers/Execution-time
done
