#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <ctime>
#include <fstream>
#include <cstring>
#include <cstdio>
using namespace std;

#define FILTER_SIZE 3
#define FILTERS 2

// Optimize this function
void applyFilters(int *image, int height, int width, int *filter, int filters, int blksize)
{
	for(int fno = 0; fno < filters; ++fno)
	{
		int *copy = new int[height * width];
		memset(copy, 0, height*width*(sizeof fno) );
		int ilim, jlim;

		//		for(int i=0; i< height; i+=blksize){
		//			ilim = (i+blksize>=height)? height: i+blksize;
		for(int j=0; j<width; j+=blksize){
			jlim = (j+blksize>=width)? width : j+blksize;
			//		for(int h = i; h < ilim; ++h)
			for(int h = 0; h < height; ++h)
			{
				for(int w = j; w < jlim; ++w)
				{
					for(int offy = -1; offy <= 1; ++offy)
					{
						if(offy + w < 0 || offy + w >= width)
							continue;
						for(int offx = -1; offx <= 1; ++offx)
						{
							if( h - offx < 0 || h - offx >= height)
								continue;
							copy[(h-offx) * width + w] += image[h * width + offy + w] * filter[fno * FILTER_SIZE * FILTER_SIZE + (1 + offx) * FILTER_SIZE + 1 + offy];
						}
					}
				}
			}
			//}
	}

	int *temp = image;
	image = copy;
	delete temp;
	}
}

int main(int argc, char** argv)
{
	int blksize;
	blksize = atoi(argv[1]);
	int ht, wd;
	cin >> ht >> wd;
	int *img = new int[ht * wd];
	for(int i = 0; i < ht; ++i)
		for(int j = 0; j < wd; ++j)
			cin >> img[i * wd + j];

	int filters = FILTERS;
	int *filter = new int[filters * FILTER_SIZE * FILTER_SIZE];
	for(int i = 0; i < filters; ++i)
		for(int j = 0; j < FILTER_SIZE; ++j)
			for(int k = 0; k < FILTER_SIZE; ++k)
				cin >> filter[i * FILTER_SIZE * FILTER_SIZE + j * FILTER_SIZE + k];

	clock_t begin = clock();
	applyFilters(img, ht, wd, filter, filters, blksize);
	clock_t end = clock();
	cout << "Execution time: " << double(end - begin) / (double)CLOCKS_PER_SEC << " seconds\n";
	ofstream fout("output.txt");
	for(int i = 0; i < ht; ++i)
	{
		for(int j = 0; j < wd; ++j)
			fout << img[i * wd + j] << " ";
		fout << "\n";
	}
}
