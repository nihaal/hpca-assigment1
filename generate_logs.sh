#!/bin/bash
for i in {1..3}
do
	rm filter
	echo "Compiling for optimization "$i
	g++ code/opt$i*/correlation_filter.cpp -o filter
	echo "Finished compiling... running perf"
	bash perf.sh opt$i
done
